-- --------------------------------------------------------
-- Host:                         soluciones-informaticas.com.co
-- Versión del servidor:         5.7.36-0ubuntu0.18.04.1 - (Ubuntu)
-- SO del servidor:              Linux
-- HeidiSQL Versión:             11.3.0.6295
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


-- Volcando estructura de base de datos para cidenet-003
CREATE DATABASE IF NOT EXISTS `cidenet-003` /*!40100 DEFAULT CHARACTER SET utf8mb4 */;
USE `cidenet-003`;

-- Volcando estructura para tabla cidenet-003.areas
DROP TABLE IF EXISTS `areas`;
CREATE TABLE IF NOT EXISTS `areas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(150) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4;

-- Volcando datos para la tabla cidenet-003.areas: ~6 rows (aproximadamente)
DELETE FROM `areas`;
/*!40000 ALTER TABLE `areas` DISABLE KEYS */;
INSERT INTO `areas` (`id`, `name`) VALUES
	(1, 'administration'),
	(2, 'financial'),
	(3, 'shopping'),
	(4, 'infrastructure\r\n'),
	(5, 'operation'),
	(6, 'human_talent'),
	(7, 'various_services');
/*!40000 ALTER TABLE `areas` ENABLE KEYS */;

-- Volcando estructura para tabla cidenet-003.countries
DROP TABLE IF EXISTS `countries`;
CREATE TABLE IF NOT EXISTS `countries` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(150) NOT NULL,
  `code` varchar(5) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `code` (`code`),
  KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;

-- Volcando datos para la tabla cidenet-003.countries: ~2 rows (aproximadamente)
DELETE FROM `countries`;
/*!40000 ALTER TABLE `countries` DISABLE KEYS */;
INSERT INTO `countries` (`id`, `name`, `code`) VALUES
	(1, 'colombia', 'co'),
	(2, 'united_states', 'us');
/*!40000 ALTER TABLE `countries` ENABLE KEYS */;

-- Volcando estructura para tabla cidenet-003.employees
DROP TABLE IF EXISTS `employees`;
CREATE TABLE IF NOT EXISTS `employees` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `first_surname` varchar(254) NOT NULL,
  `last_surname` varchar(254) NOT NULL,
  `first_name` varchar(254) NOT NULL,
  `last_name` varchar(254) NOT NULL,
  `country` int(11) NOT NULL,
  `identification_type` int(11) NOT NULL,
  `identification_number` varchar(50) NOT NULL,
  `email` varchar(300) NOT NULL,
  `date_admission` date NOT NULL,
  `area` int(11) NOT NULL,
  `created` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`),
  KEY `id` (`id`),
  KEY `FK_employees_types_identifications` (`identification_type`),
  KEY `FK_employees_areas` (`area`),
  KEY `FK3` (`country`),
  CONSTRAINT `FK3` FOREIGN KEY (`country`) REFERENCES `countries` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_employees_areas` FOREIGN KEY (`area`) REFERENCES `areas` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_employees_types_identifications` FOREIGN KEY (`identification_type`) REFERENCES `types_identifications` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;

-- Volcando datos para la tabla cidenet-003.employees: ~1 rows (aproximadamente)
DELETE FROM `employees`;
/*!40000 ALTER TABLE `employees` DISABLE KEYS */;
INSERT INTO `employees` (`id`, `first_surname`, `last_surname`, `first_name`, `last_name`, `country`, `identification_type`, `identification_number`, `email`, `date_admission`, `area`, `created`, `updated`) VALUES
	(1, 'Gomez', 'Maya', 'Andres', 'Felipe', 1, 2, '1035429360', 'andres.gomez@cidenet.com.co', '2022-01-19', 1, '2022-01-19 20:57:01', '2022-01-19 20:57:01'),
	(2, 'Gomez', 'Maya', 'Andres', 'Felipe', 1, 1, 'CC-1035429360', 'andres.gomez.2@cidenet.com.co', '2022-01-04', 1, '2022-01-19 23:33:29', '2022-01-19 23:33:29');
/*!40000 ALTER TABLE `employees` ENABLE KEYS */;

-- Volcando estructura para tabla cidenet-003.types_identifications
DROP TABLE IF EXISTS `types_identifications`;
CREATE TABLE IF NOT EXISTS `types_identifications` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(150) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4;

-- Volcando datos para la tabla cidenet-003.types_identifications: ~4 rows (aproximadamente)
DELETE FROM `types_identifications`;
/*!40000 ALTER TABLE `types_identifications` DISABLE KEYS */;
INSERT INTO `types_identifications` (`id`, `name`) VALUES
	(1, 'citizenship_card'),
	(2, 'foreigner_id'),
	(3, 'passport'),
	(4, 'special_permission');
/*!40000 ALTER TABLE `types_identifications` ENABLE KEYS */;

-- Volcando estructura para tabla cidenet-003.users
DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(32) NOT NULL,
  `password` text NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`),
  KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;

-- Volcando datos para la tabla cidenet-003.users: ~0 rows (aproximadamente)
DELETE FROM `users`;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`id`, `username`, `password`) VALUES
	(1, 'admin', '$2y$10$mWkPKKf6bqbxuBXaMCyXxucOnaXmvYLHWC.327dqglaK5njVzSCWu');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
