<?php
return array(
  'driver' => "mysql",
  'debug' => true,
  'database' => 'cidenet-003',
  'username' => 'cidenet',
  'password' => 'cidenet2022',
  'basePath' => "/api.php",
  'middlewares' => 'cors,sslRedirect,ajaxOnly,dbAuth,pageLimits,authorization,sanitation,validation,customization,multiTenancy', // xsrf , joinLimits
  # 'dbAuth.mode' => 'required',
  'dbAuth.mode' => 'optional',
  'dbAuth.usersTable' => "users",
  'dbAuth.usernameColumn' => 'username',
  'dbAuth.passwordColumn' => 'password',
  'dbAuth.returnedColumns' => '',
  'dbAuth.registerUser' => 1,
  'dbAuth.passwordLength' => 8,
  'pageLimits.pages' => 100,
  'pageLimits.records' => -1,
  // 'joinLimits.depth' => 100,
  // 'joinLimits.tables' => 100,
  // 'joinLimits.records' => 1000,
  'sanitation.types' => 'date,timestamp',
  'openApiBase' => '{"info":{"title":"PACMEC API","version":"1.0.0"}}',
  //'sanitation.tables' => 'posts,comments',
  'customization.beforeHandler' => function ($operation, $tableName, $request, $environment) {
    $environment->start = microtime(true);
  },
  'customization.afterHandler' => function ($operation, $tableName, $response, $environment) {
    return $response->withHeader('X-Time-Taken', microtime(true) - $environment->start);
  }
);
?>
