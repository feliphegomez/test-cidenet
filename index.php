<html lang="es">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>VUE-CRUD-UI</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/css/bootstrap.min.css" />
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/datepicker/1.0.10/datepicker.min.css" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/vue/2.6.10/vue.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/vue-router/3.0.2/vue-router.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.18.0/axios.js"></script>
    <style>
      .bd-placeholder-img {
        font-size: 1.125rem;
        text-anchor: middle;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
      }

      @media (min-width: 768px) {
        .bd-placeholder-img-lg {
          font-size: 3.5rem;
        }
      }
      /* Show it is fixed to the top */
      body {
        min-height: 75rem;
        padding-top: 4.5rem;
      }
    </style>
  </head>
  <body>
    <main id="app">
      <!-- // <menu-component v-if="definition!==null" :subjects="definition.tags"></menu-component> -->
      <navbar-top-component v-if="definition!==null" :subjects="definition.tags"></navbar-top-component>
      <router-view :key="$route.fullPath" v-if="definition!==null" :definition="definition"></router-view>
    </main>

    <template id="navbar-top">
      <nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
        <a class="navbar-brand" href="#">CideNet</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarCollapse">
          <ul class="navbar-nav mr-auto">
            <!-- <li class="nav-item"><router-link v-bind:to="{ name: 'Home' }" class="nav-link" >Inicio</router-link></li> -->
            <li class="nav-item"><router-link class="nav-link" v-bind:to="{ name: 'List', params: { subject: 'employees' } }">Ver todos</router-link></li>
            <li class="nav-item"><router-link class="nav-link" v-bind:to="{ name: 'Add', params: { subject: 'employees' } }">Registrar</router-link></li>
            <!--
            <li class="nav-item" v-for="subject in subjects">
              <router-link v-bind:to="{name: 'List', params: {subject: subject.name}}" class="nav-link" :key="subject.name">
                {{ subject.name }}
              </router-link>
            </li>
            -->
          </ul>
        </div>
      </nav>
    </template>

    <template id="create">
      <div class="container">
        <!-- //
          <div class="py-5 text-center">
            <h2>{{ subject }} - add</h2>
            <p class="lead" v-if="record !== null">
            {{ record }}
            </p>
          </div>
        -->
        <hr class="mb-4">
        <div class="row">
          <div class="col-md-12 order-md-3">
            <h4 class="mb-3">Formulario de Registro</h4>
            <form class="needs-validation" novalidate_ action="javascript:return false;" v-on:submit="createRecord" >
              <div class="row">
                <div class="col-md-6 mb-3">
                  <label for="first_surname">Primer Apellido</label>
                  <input v-if="references['first_surname'] === false" class="form-control" id="first_surname" v-model="record.first_surname" required />
                  <small class="text-muted">Solo permite caracteres de la A a la Z, mayúscula, sin acentos ni Ñ. Es requerido y su longitud máxima será de 20 letras.</small>
                  <div class="invalid-feedback">Valid first name is required.</div>
                </div>
                <div class="col-md-6 mb-3">
                  <label for="last_surname">Segundo Apellido</label>
                  <input type="text" class="form-control" placeholder="" id="last_surname" v-model="record.last_surname" required />
                  <small class="text-muted">Solo permite caracteres de la A a la Z, mayúscula, sin acentos ni Ñ. Es requerido y su longitud máxima será de 20 letras.</small>
                  <div class="invalid-feedback">Valid last name is required.</div>
                </div>
              </div>

              <div class="row">
                <div class="col-md-6 mb-3">
                  <label for="first_name">Primer Nombre</label>
                  <input type="text" class="form-control" placeholder="" id="first_name" v-model="record.first_name" required />
                  <small class="text-muted">Solo permite caracteres de la A a la Z, mayúscula, sin acentos ni Ñ Es requerido y su longitud máxima será de 20 letras.</small>
                  <div class="invalid-feedback">Valid first name is required.</div>
                </div>
                <div class="col-md-6 mb-3">
                  <label for="last_name">Otros Nombres</label>
                  <input type="text" class="form-control" placeholder="" id="last_name" v-model="record.last_name" required />
                  <small class="text-muted">Solo permite caracteres de la A a la Z, mayúscula, sin acentos ni Ñ, y el caracter espacio entre nombres. Es opcional y su longitud máxima será de 50 letras.</small>
                  <div class="invalid-feedback">Valid last name is required.</div>
                </div>
              </div>

              <div class="row">
                <div class="col-md-4 mb-3">
                  <label for="country">País del empleo</label>
                  <select @change="genEmail" class="custom-select d-block w-100" v-model="record.country" required="">
                    <option value="">Elegir</option>
                    <option v-for="option in options[references.country]" v-bind:value="option.key">{{ option.value }}</option>
                  </select>
                  <small class="text-muted">País para el cual el empleado prestará sus servicios, podrá ser Colombia o Estados Unidos.</small>
                  <div class="invalid-feedback">Please select a valid country.</div>
                </div>
                <div class="col-md-4 mb-3">
                  <label for="identification_type">Tipo de Identificación</label>
                  <select class="custom-select d-block w-100" v-model="record.identification_type" required="">
                    <option value="">Elegir</option>
                    <option v-for="option in options[references.identification_type]" v-bind:value="option.key">{{ option.value }}</option>
                  </select>
                  <small class="text-muted">Será el tipo de identificación, por ejemplo, Cédula de Ciudadanía, Cédula de Extranjería, Pasaporte, Permiso Especial. Debe ser una lista desplegable en la que el usuario seleccione el tipo de identificación correspondiente.</small>
                  <div class="invalid-feedback">Please provide a valid state.</div>
                </div>
                <div class="col-md-4 mb-3">
                  <label for="identification_number">Número de Identificación</label>
                  <input type="text" class="form-control" placeholder="" value="" id="identification_number" v-model="record.identification_number" required />
                  <small class="text-muted">Es el número de identificación del empleado, debe ser alfanumérico permitiendo los siguientes conjuntos de caracteres (a-z / A-Z / 0-9 / -). No podrán existir dos empleados con el mismo número y tipo de identificación. Su longitud máxima serán 20 caracteres.</small>
                  <div class="invalid-feedback">Zip code required.</div>
                </div>
              </div>

              <div class="mb-3">
                <label for="email">Correo electrónico <span class="text-muted">(* Requerido)</span></label>
                <input type="text" class="form-control" placeholder="you@example.com" id="email" v-model="record.email" readonly required />
                <small class="text-muted">&lt;PRIMER_NOMBRE&gt;.&lt;PRIMER_APELLIDO&gt;.&lt;ID>@&lt;DOMINIO&gt;.</small>
                <small class="text-muted">El DOMINIO será cidenet.com.co para Colombia y cidenet.com.us para Estados Unidos.</small>
                <div class="invalid-feedback">Please enter a valid email address for shipping updates.</div>
              </div>

              <div class="row">
                <div class="col-md-4 mb-3">
                  <label for="date_admission">Fecha de ingreso</label>
                  <input type="text" class="form-control" placeholder="" id="date_admission" v-model="record.date_admission" required />
                  <small class="text-muted">No podrá ser superior a la fecha actual, pero sí podrá ser hasta un mes menor, ya que es posible que el usuario no haya podido registrarlo el día en que ingresó.</small>
                  <div class="invalid-feedback">Valid first name is required.</div>
                </div>
                <div class="col-md-4 mb-3">
                  <label for="area">Área</label>
                  <select class="custom-select d-block w-100" v-model="record.area" required="">
                    <option value="">Elegir</option>
                    <option v-for="option in options[references.area]" v-bind:value="option.key">{{ option.value }}</option>
                  </select>
                  <small class="text-muted">Será el área para la cual fue contratado el empleado, puede ser, Administración, Financiera, Compras, Infraestructura, Operación, Talento Humano, Servicios Varios, etc. Debe ser una lista desplegable en la que el usuario seleccione el área correspondiente.</small>
                  <div class="invalid-feedback">Please provide a valid state.</div>
                </div>
                <div class="col-md-4 mb-3">
                  <label for="created">Fecha y hora de registro</label>
                  <input type="text" class="form-control" placeholder="" id="created" :value="current_date" readonly />
                  <small class="text-muted">Mostrará la fecha y hora del registro en formato DD/MM/YYYY HH:mm:ss, no puede ser editado.</small>
                  <div class="invalid-feedback">Valid last name is required.</div>
                </div>
              </div>

              <hr class="mb-4">
              <button class="btn btn-primary btn-lg btn-block" type="submit">Crear</button>

              <!-- //
                <template v-for="(value, key) in record">
                  <div class="form-group">
                    <label v-bind:for="key">{{ key }}</label>
                    <input v-if="references[key] === false" class="form-control" v-bind:id="key" v-model="record[key]" :disabled="key === primaryKey" />
                    <select v-else class="form-control" v-bind:id="key" v-model="record[key]">
                      <option value=""></option>
                      <option v-for="option in options[references[key]]" v-bind:value="option.key">{{ option.value }}</option>
                    </select>
                  </div>
                </template>
              -->

            </form>
          </div>
        </div>
    </template>

    <template id="menu">
      <div v-if="subjects!==null" class="nav flex-column nav-pills">
          <router-link v-for="subject in subjects" v-bind:to="{name: 'List', params: {subject: subject.name}}" class="nav-link" :key="subject.name">
            {{ subject.name }}
          </router-link>
      </div>
    </template>

    <template id="home">
      <div>Nothing</div>
    </template>

    <template id="list">
      <div class="container">
        <hr class="mb-4">
        <div class="row">
          <div class="col-md-12 order-md-3">
            <h4 class="mb-3">Consulta - {{ subject }}</h4>
            <p>
              <router-link class="btn btn-primary" v-bind:to="{name: 'Add', params: {subject: subject}}">
                Add
              </router-link>
            </p>
          </div>
          <div class="col-md-12 order-md-3">
            <div class="card bg-light" v-if="field"><div class="card-body">
              <div style="float:right;"><router-link v-bind:to="{name: 'List', params: {subject: subject}}">Clear filter</router-link></div>
              <p class="card-text">Filtered by: {{ field }} = {{ id }}</p>
            </div></div>
            <p v-if="records===null">Loading...</p>
            <div v-else class="table-responsive">
              <table class="table table-striped table-sm">
                <thead>
                  <tr>
                    <th v-for="value in Object.keys(properties)">{{ value }}</th>
                    <th v-if="related">related</th>
                    <th v-if="primaryKey">actions</th>
                  </tr>
                </thead>
                <tbody>
                  <tr v-for="record in records">
                    <template v-for="(value, key) in record">
                      <td v-if="references[key] !== false">
                        <router-link v-bind:to="{name: 'View', params: {subject: references[key], id: referenceId(references[key], record[key])}}">{{ referenceText(references[key], record[key]) }}</router-link>
                      </td>
                      <td v-else>{{ value }}</td>
                    </template>
                    <td v-if="related">
                      <template v-for="(relation, i) in referenced">
                        <router-link v-bind:to="{name: 'Filter', params: {subject: relation[0], field: relation[1], id: record[primaryKey]}}">{{ relation[0] }}</router-link>&nbsp;
                      </template>
                    </td>
                    <td v-if="primaryKey" style="padding: 6px; white-space: nowrap;">
                      <router-link class="btn btn-secondary btn-sm" v-bind:to="{name: 'View', params: {subject: subject, id: record[primaryKey]}}">View</router-link>
                      <!--//
                        <router-link class="btn btn-secondary btn-sm" v-bind:to="{name: 'Edit', params: {subject: subject, id: record[primaryKey]}}">Edit</router-link>
                        <router-link class="btn btn-danger btn-sm" v-bind:to="{name: 'Delete', params: {subject: subject, id: record[primaryKey]}}">Delete</router-link>
                      -->
                    </td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </template>

    <template id="record-create">
      <div>
        <h2>{{ subject }} - add</h2>
        <form v-on:submit="createRecord" action="javascript:false;">
          <template v-for="(value, key) in record">
            <div class="form-group">
              <label v-bind:for="key">{{ key }}</label>
              <input v-if="references[key] === false" class="form-control" v-bind:id="key" v-model="record[key]" :disabled="key === primaryKey" />
              <select v-else class="form-control" v-bind:id="key" v-model="record[key]">
                <option value=""></option>
                <option v-for="option in options[references[key]]" v-bind:value="option.key">{{ option.value }}</option>
              </select>
            </div>
          </template>
          <button type="submit" class="btn btn-primary">Create</button>
          <router-link class="btn btn-primary" v-bind:to="{name: 'List', params: {subject: subject}}">Cancel</router-link>
        </form>
      </div>
    </template>

    <template id="view">
      <div>
        <h2>{{ subject }} - view</h2>
        <p v-if="record===null">Loading...</p>
        <dl v-else>
          <template v-for="(value, key) in record">
            <dt>{{ key }} </dt>
            <dd>{{ value }}</dd>
          </template>
        </dl>
      </div>
    </template>

    <template id="update">
      <div>
        <h2>{{ subject }} - edit</h2>
        <p v-if="record===null">Loading...</p>
        <form v-else v-on:submit="updateRecord">
          <template v-for="(value, key) in record">
            <div class="form-group">
              <label v-bind:for="key">{{ key }}</label>
              <input v-if="references[key] === false" class="form-control" v-bind:id="key" v-model="record[key]" :disabled="key === primaryKey" />
              <select v-else-if="!options[references[key]]" class="form-control" disabled>
                <option value="" selected>Loading...</option>
              </select>
              <select v-else class="form-control" v-bind:id="key" v-model="record[key]">
                <option value=""></option>
                <option v-for="option in options[references[key]]" v-bind:value="option.key">{{ option.value }}</option>
              </select>
            </div>
          </template>
          <button type="submit" class="btn btn-primary">Save</button>
          <router-link class="btn btn-secondary" v-bind:to="{name: 'List', params: {subject: subject}}">Cancel</router-link>
        </form>
      </div>
    </template>

    <template id="delete">
      <div>
        <h2>{{ subject }} delete #{{ id }}</h2>
        <form v-on:submit="deleteRecord">
          <p>The action cannot be undone.</p>
          <button type="submit" class="btn btn-danger">Delete</button>
          <router-link class="btn btn-secondary" v-bind:to="{name: 'List', params: {subject: subject}}">Cancel</router-link>
        </form>
      </div>
    </template>

    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.inputmask/5.0.8-beta.1/jquery.inputmask.min.js" integrity="sha512-0o4sWzxULgmULBKVb/9AuSGWVebaP2gmz3XPCI8OvTa9z/A6mzCBUjWj7birew55lAimOwaqd0eIGdeueiiLDQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/datepicker/1.0.10/datepicker.min.js" integrity="sha512-RCgrAvvoLpP7KVgTkTctrUdv7C6t7Un3p1iaoPr1++3pybCyCsCZZN7QEHMZTcJTmcJ7jzexTO+eFpHk4OCFAg==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <script>
      var OpenAPI = null;
      var router = null;
      var api = axios.create({
        baseURL: 'api.php',
        withCredentials: true
      });

      api.interceptors.response.use(function (response) {
        if (response.headers['x-xsrf-token']) {
          document.cookie = 'XSRF-TOKEN=' + response.headers['x-xsrf-token'] + '; path=/';
        }
        return response;
      });

      var util = {
        methods: {
          resolve: function (path, obj) {
            return path.reduce(function(prev, curr) {
              return prev ? prev[curr] : undefined
            }, obj || this);
          },
          getDisplayColumn: function (columns) {
            var index = -1;
            var names = ['name', 'title', 'description', 'username'];
            for (var i in names) {
              index = columns.indexOf(names[i]);
              if (index >= 0) {
                return names[i];
              }
            }
            return columns[0];
          },
          getPrimaryKey: function (properties) {
            for (var key in properties) {
              if (properties[key]['x-primary-key']) {
                return key;
              }
            }
            return false;
          },
          getReferenced: function (properties) {
            var referenced = [];
            for (var key in properties) {
              if (properties[key]['x-referenced']) {
                for (var i = 0; i < properties[key]['x-referenced'].length; i++) {
                  referenced.push(properties[key]['x-referenced'][i].split('.'));
                }
              }
            }
            return referenced;
          },
          getReferences: function (properties) {
            var references = {};
            for (var key in properties) {
              if (properties[key]['x-references']) {
                references[key] = properties[key]['x-references'];
              } else {
                references[key] = false;
              }
            }
            return references;
          },
          getProperties: function (action, subject, definition) {
            if (action == 'list') {
              path = ['components', 'schemas', action + '-' + subject, 'properties', 'records', 'items', 'properties'];
            } else {
              path = ['components', 'schemas', action + '-' + subject, 'properties'];
            }
            return this.resolve(path, definition);
          }
        }
      };

      var orm = {
        methods: {
          readRecord: function () {
            this.id = this.$route.params.id;
            this.subject = this.$route.params.subject;
            this.record = null;
            var self = this;
            api.get('/records/' + this.subject + '/' + this.id).then(function (response) {
              self.record = response.data;
            }).catch(function (error) {
              console.log(error);
            });
          },
          readRecords: function () {
            this.subject = this.$route.params.subject;
            this.records = null;
            var url = '/records/' + this.subject;
            var params = [];
            for (var i=0;i<this.join.length;i++) {
              params.push('join='+this.join[i]);
            }
            if (this.field) {
              params.push('filter='+this.field+',eq,'+this.id);
            }
            if (params.length>0) {
              url += '?'+params.join('&');
            }
            var self = this;
            api.get(url).then(function (response) {
              self.records = response.data.records;
            }).catch(function (error) {
              console.log(error);
            });
          },
          readOptions: function() {
            this.options = {};
            var self = this;
            for (var key in this.references) {
              var subject = this.references[key];
              if (subject !== false) {
                var properties = this.getProperties('list', subject, this.definition);
                var displayColumn = this.getDisplayColumn(Object.keys(properties));
                var primaryKey = this.getPrimaryKey(properties);
                api.get('/records/' + subject + '?include=' + primaryKey + ',' + displayColumn).then(function (subject, primaryKey, displayColumn, response) {
                  self.options[subject] = response.data.records.map(function (record) {
                    return {key: record[primaryKey], value: record[displayColumn]};
                  });
                  self.$forceUpdate();
                }.bind(null, subject, primaryKey, displayColumn)).catch(function (error) {
                  console.log(error);
                });
              }
            }
          },
          updateRecord: function () {
            api.put('/records/' + this.subject + '/' + this.id, this.record).then(function (response) {
              console.log(response.data);
            }).catch(function (error) {
              console.log(error);
            });
            router.push({name: 'List', params: {subject: this.subject}});
          },
          initRecord: function () {
            this.record = {};
            for (var key in this.properties) {
              if (!this.properties[key]['x-primary-key']) {
                if (this.properties[key].default) {
                  this.record[key] = this.properties[key].default;
                } else {
                  this.record[key] = '';
                }
              }
            }
          },
          createRecord: function() {
            var self = this;
            api.post('/records/' + this.subject, this.record).then(function (response) {
              self.record.id = response.data;
            }).catch(function (error) {
              console.log(error);
            });
            router.push({name: 'List', params: {subject: this.subject}});
          },
          deleteRecord: function () {
            api.delete('/records/' + this.subject + '/' + this.id).then(function (response) {
              console.log(response.data);
            }).catch(function (error) {
              console.log(error);
            });
            router.push({name: 'List', params: {subject: this.subject}});
          }
        }
      };

      Vue.component('menu-component', {
        mixins: [util, orm],
        template: '#menu',
        props: ['subjects']
      });

      Vue.component('navbar-top-component', {
        mixins: [util, orm],
        template: '#navbar-top',
        props: ['subjects']
      });

      var Home = Vue.extend({
        mixins: [util],
        template: '#home'
      });

      var Add = Vue.extend({
        mixins: [util, orm],
        template: '#create',
        props: ['definition'],
        data: function () {
          return {
            id: this.$route.params.id,
            subject: this.$route.params.subject,
            record: {
              first_surname: '',
              last_surname: '',
              first_name: '',
              last_name: '',
              country: '',
              identification_type: '',
              identification_number: '',
              email: '',
              date_admission: '',
              area: '',
            },
            current_date: null,
            options: {}
          };
        },
        created: function () {
          //this.initRecord();
          this.readOptions();
        },
        computed: {
          properties: function () {
            return this.getProperties('create', this.subject, this.definition);
          },
          primaryKey: function () {
            return this.getPrimaryKey(this.properties);
          },
          references: function () {
            return this.getReferences(this.properties);
          },
        },
        mounted(){
          let self = this;
          self.timer();
          $("#first_surname").inputmask({"regex": "[a-zA-Z ]{20}", "placeholder": ""}).on('change', function() {
            // console.log(" val: " + $("#first_surname").val());
            self.record.first_surname = this.value;
            self.genEmail();
          });
          $("#last_surname").inputmask({"regex": "[a-zA-Z]{20}", "placeholder": ""}).on('change', function() {
            self.record.last_surname = this.value;
          });
          $("#first_name").inputmask({"regex": "[a-zA-Z]{20}", "placeholder": ""}).on('change', function() {
            self.record.first_name = this.value;
            self.genEmail();
          });
          $("#last_name").inputmask({"regex": "[a-zA-Z ]{50}", "placeholder": ""}).on('change', function() {
            self.record.last_name = this.value;
          });
          $("#identification_number").inputmask({"regex": "[a-zA-Z0-9-]{20}", "placeholder": ""}).on('change', function() {
            self.record.identification_number = this.value;
          });
          $("#date_admission").datepicker({
            // date: new Date(2014, 1, 14) // Or '02/14/2014'
            // language: 'es-ES',
            format: 'yyyy-mm-dd',
            filter: function(date, view) {
                var registro = new Date;
                if (date > registro) return false;
                var d = new Date();
                d.setMonth(registro.getMonth() - 1);
                if (date < d) return false;

                let day   = date.getDate();
                let month = date.getMonth() + 1;
                let year  = date.getFullYear();

                if(month < 10) t_str = `${year}-0${month}-${day}`;
                else t_str = `${year}-${month}-${day}`;
                $("#date_admission").val(t_str);
                self.date_admission = t_str;
            }
          }).on('change', function (e) { self.record.date_admission = this.value; });
          /*
          .on('pick.datepicker', function (e) {
            console.log(e.date);

            if (e.date < new Date()) {
              e.preventDefault(); // Prevent to pick the date
            }
          });
          */
        },
        methods: {
          timer(){
            let self = this;
            var currentTime = new Date();
            let day         = currentTime.getDate();
            let month       = currentTime.getMonth() + 1;
            let year        = currentTime.getFullYear();
            var hours       = currentTime.getHours();
            var minutes     = currentTime.getMinutes();
            var sec         = currentTime.getSeconds();
            if(month < 10) t_str = `${year}-0${month}-${day}`;
            else t_str = `${year}-${month}-${day}`;
            if (minutes < 10) minutes = "0" + minutes;
            if (sec < 10) sec = "0" + sec;
            t_str += ' ' + hours + ":" + minutes + ":" + sec + " ";
            self.current_date = t_str;
            setTimeout(self.timer,60000);
          },
          formSubmit(){
            let self = this;
            console.log(self.record);
          },
          genEmail(){
            let self = this;
            if(self.record.country <= 0 || self.record.country == '') return;
            if(self.record.first_name.length <= 0) return;
            if(self.record.first_surname.length <= 0) return;
            let f_n = self.record.first_name;
            let f_s = self.record.first_surname;
            let countries = self.options[self.references.country];
            let c_id = self.record.country;
            let country = countries.find(c => c.key === c_id);
            let domain = (country.value == 'colombia') ? 'cidenet.com.co' : 'cidenet.com.us';
            let username = (f_n + '.' + f_s).replace(/\s+/g, '').toLowerCase();

            api.get('/records/' + this.subject, {
              params: {
                filter: [
                  'country,eq,' + c_id,
                  'email,cs,' + username
                ]
              }
            }).then(function (response) {
              console.log(response);
              let email = username + (response.data.records.length > 0 ? '.'+(response.data.records.length+1) : '') + '@' + domain;
              console.log(email);
              self.record.email = email;

            }).catch(function (error) {
              console.log(error);
            });

            return email;
          },
        }
      });

      var List = Vue.extend({
        mixins: [util, orm],
        template: '#list',
        data: function () {
          return {
            records: null,
            subject: this.$route.params.subject,
            field: this.$route.params.field,
            id: this.$route.params.id
          };
        },
        props: ['definition'],
        created: function () {
          this.readRecords();
        },
        computed: {
          related: function () {
            return (this.referenced.filter(function (value) { return value; }).length > 0);
          },
          join: function () {
            return Object.values(this.references).filter(function (value) { return value; });
          },
          properties: function () {
            return this.getProperties('list', this.subject, this.definition);
          },
          references: function () {
            return this.getReferences(this.properties);
          },
          referenced: function () {
            return this.getReferenced(this.properties);
          },
          primaryKey: function () {
            return this.getPrimaryKey(this.properties);
          }
        },
        methods: {
          referenceText(subject, record) {
            var properties = this.getProperties('read', subject, this.definition);
            var displayColumn = this.getDisplayColumn(Object.keys(properties));
            return record[displayColumn];
          },
          referenceId(subject, record) {
            var properties = this.getProperties('read', subject, this.definition);
            var primaryKey = this.getPrimaryKey(properties);
            return record[primaryKey];
          }
        }
      });

      var View = Vue.extend({
        mixins: [util, orm],
        template: '#view',
        props: ['definition'],
        data: function () {
          return {
            id: this.$route.params.id,
            subject: this.$route.params.subject,
            record: null
          };
        },
        created: function () {
          this.readRecord();
        },
        computed: {
          properties: function () {
            return this.getProperties('read', this.subject, this.definition);
          }
        },
        methods: {
        }
      });

      var Edit = Vue.extend({
        mixins: [util, orm],
        template: '#update',
        props: ['definition'],
        data: function () {
          return {
            id: this.$route.params.id,
            subject: this.$route.params.subject,
            record: null,
            options: {}
          };
        },
        created: function () {
          this.readRecord();
          this.readOptions();
        },
        computed: {
          properties: function () {
            return this.getProperties('update', this.subject, this.definition);
          },
          primaryKey: function () {
            return this.getPrimaryKey(this.properties);
          },
          references: function () {
            return this.getReferences(this.properties);
          },
        },
        methods: {
        }
      });

      var Delete = Vue.extend({
        mixins: [util, orm],
        template: '#delete',
        data: function () {
          return {
            id: this.$route.params.id,
            subject: this.$route.params.subject
          };
        },
        methods: {
        }
      });

      var RecordAdd = Vue.extend({
        mixins: [util, orm],
        template: '#create',
        props: ['definition'],
        data: function () {
          return {
            id: this.$route.params.id,
            subject: this.$route.params.subject,
            record: null,
            options: {}
          };
        },
        created: function () {
          this.initRecord();
          this.readOptions();
        },
        computed: {
          properties: function () {
            return this.getProperties('create', this.subject, this.definition);
          },
          primaryKey: function () {
            return this.getPrimaryKey(this.properties);
          },
          references: function () {
            return this.getReferences(this.properties);
          }
        },
        methods: {
        }
      });

        router = new VueRouter({
          linkActiveClass: 'active',
          routes:[
            { path: '/', component: Home, name: 'Home' },
            { path: '/:subject/create', component: Add, name: 'Add' },
            // { path: '/employees/create', component: Add, name: 'Add' },

            { path: '/:subject/read/:id', component: View, name: 'View' },
            { path: '/:subject/update/:id', component: Edit, name: 'Edit' },
            { path: '/:subject/delete/:id', component: Delete, name: 'Delete' },
            { path: '/:subject/list', component: List, name: 'List' },
            { path: '/:subject/list/:field/:id', component: List, name: 'Filter' }
          ]
        });

        var app = new Vue({
          router: router,
          data: function () {
            return {
              definition: null,
              options: {},
            };
          },
          created: function () {
            var self = this;
            // self.definition = OpenAPI;
            api.get('/openapi').then(function (response) {
              self.definition = response.data;
            }).catch(function (error) {
              console.log(error);
            });
          },
          methods: {

          }
        }).$mount('#app');

      /**
       * Run API
      **/
      /*
      var authUrl = 'auth.php'; // url of 'auth.php' from php-api-auth
      var clientId = 'default'; // client id as defined in php-api-auth
      var audience = 'api.php'; // api audience as defined in php-api-auth
      window.onload = function () {
        var match = RegExp('[#&]access_token=([^&]*)').exec(window.location.hash);
        var accessToken = match && decodeURIComponent(match[1].replace(/\+/g, ' '));
        if (!accessToken) {
          document.location = authUrl+'?audience='+audience+'&response_type=token&client_id='+clientId+'&redirect_uri='+document.location.href;
        } else {
          document.location.hash = '';
          var req = new XMLHttpRequest();
          req.onreadystatechange = function () {
              if (req.readyState==4) {
                let outJSON = JSON.parse(req.responseText);
                  console.log(outJSON);
                  OpenAPI = outJSON;
                  // document.getElementById('output').innerHTML = JSON.stringify(JSON.parse(req.responseText), undefined, 4);
                  run_api();
              }
          }
          url = 'api.php/openapi';
          req.open("GET", url, true);
          req.setRequestHeader('X-Authorization', 'Bearer '+accessToken);
          req.send();
        }
      };
      */
    </script>
  </body>
</html>
